package com.kangmifhd.navigationcomponent

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.kangmifhd.navigationcomponent.databinding.FragmentViewTransactionBinding

class ViewTransactionFragment : Fragment() {

    private lateinit var binding: FragmentViewTransactionBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentViewTransactionBinding.bind(view)
    }
}