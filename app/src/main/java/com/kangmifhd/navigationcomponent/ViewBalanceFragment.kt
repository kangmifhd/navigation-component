package com.kangmifhd.navigationcomponent

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.kangmifhd.navigationcomponent.databinding.FragmentViewBalanceBinding

class ViewBalanceFragment : Fragment() {

    private lateinit var binding: FragmentViewBalanceBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentViewBalanceBinding.bind(view)
    }
}